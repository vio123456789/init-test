//add 
/**
 * Adds two numbers.
 * @param {number} a The first addend.
 * @param {number} b The second addend.
 * @returns {number} The sum of the addends.
 */
const add = (a, b) => a + b;

/**
 * Subtracts one number from another.
 * @param {number} minuend The number to be subtracted from.
 * @param {number} subtrahend The number to subtract.
 * @returns {number} The difference of the numbers.
 */
const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

/**
 * Multiplies two numbers.
 * @param {number} multiplier1 The first factor.
 * @param {number} multiplier2 The second factor.
 * @returns {number} The product of the factors.
 */

const multiply = (multiplier1, multiplier2) => {
    return multiplier1 * multiplier2;
};

/**
 * Divides one number by another.
 * @param {number} dividend The number to be divided.
 * @param {number} divisor The number to divide by.
 * @returns {number} The quotient of the division.
 * @throws {Error} when divisor is 0
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) {
        throw new Error("Can't divide by zero");
    }
    const fraction = dividend / divisor;
    return fraction;
};

export default { add, subtract, multiply, divide };
