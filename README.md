
## Project Setup Steps

1. Create project directory e.g.,
   ```bash
   mkdir -p ~/projects/stam/LO3/unit-test
   ```
2. Open in code editor:
   ```bash
   code ~/projects/stam/LO3/unit-test
   ```
3. Open terminal and initialize as git repository:
   ```bash
   git init
   ```
4. Initialize as npm project:
   ```bash
   npm init -y
   ```
5. Install dependencies:
   ```bash
   npm i --save express
   ```
6. Install development dependencies:
   ```bash
   npm i --save-dev mocha chai
   ```
7. Create .gitignore:
   ```bash
   echo "node_modules" > .gitignore
   ```
8. Add remote repository address (replace `{username}` with your actual GitLab username):
   ```bash
   git remote add origin https://gitlab.com/{username}/unit-test
## Start the application:

```bash
npm start
```


Your app should now be running on [http://localhost:3000](http://localhost:3000).

## Running the tests

Explain how to run the automated tests for this system:

```bash
npm run test
```