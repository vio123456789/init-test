import {assert, expect, should} from 'chai';
import {describe, it} from 'mocha';
import calc from '../src/calc.js';

describe("calc.js", () => {
    before(() => {
        console.log("Starting test suite for calc module...");
    });

    after(() => {
        console.log("Test suite for calc module completed.");
    });

    it('should add two numbers correctly', () => {
        expect(calc.add(1, 2)).to.equal(3);
        expect(calc.add(2, 1)).to.equal(3);
    });

    it('should subtract two numbers correctly', () => {
        assert.equal(calc.subtract(5, 1), 4, '5 subtracted by 1 should equal 4');
        assert.equal(calc.subtract(5, 5), 0, '5 subtracted by 5 should equal 0');
    });

    it('should multiply two numbers correctly', () => {
        should().exist(calc.multiply(5, 5));
        should().exist(calc.multiply(5, 0));
    });

    it('should divide two numbers correctly', () => {
        expect(calc.divide(2, 4)).to.equal(0.5);
    });

    it('should throw an error when dividing by zero', () => {
        expect(() => calc.divide(2, 0)).to.throw("Can't divide by zero");
    });

    it('should also throw an error when dividing by zero', () => {
        expect(() => calc.divide(1, 0)).to.throw("Can't divide by zero");
    });
});
